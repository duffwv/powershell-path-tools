﻿Function Set-HanldeEULA
{
    [cmdletbinding()]

    Param(
        [Parameter( Mandatory = $True,
                    Position  = 0      )]
        [ValidateSet(
            'Accepted',
            'Declined'  )]
        [String]
        $State
    )

    Begin
    {
        $State_Lookup = @{
            'Accepted' = 1
            'Declined' = 0
        }

        $EULA_Key = 'HKCU:\Software\Sysinternals\Handle'
        $EULA_Value = 'EulaAccepted'
    }

    End
    {
        If(-not (Test-Path -Path $EULA_Key) )
        {
            New-Item -Path $EULA_Key -ItemType Directory -Force |
            Out-Null
        }

        Try
        {
            Get-ItemProperty -Path $EULA_Key -Name $EULA_Value -ErrorAction Stop |
            Out-Null

            Set-ItemProperty -Path $EULA_Key -Name $EULA_Value -Value $State_Lookup[$State]
        }
        Catch
        {
            New-ItemProperty -Path $EULA_Key -Name $EULA_Value -Value $State_Lookup[$State]
        }
    }
}

Function Get-HandleExe
{
    [cmdletbinding()]

    Param(
        [String]
        $URL = 'https://live.sysinternals.com/handle.exe',

        [String]
        $Destination = '.\',

        [Switch]
        $Force
    )

    Begin
    {
        $path = Join-Path -Path $Destination -ChildPath 'handle.exe'
        If( Test-Path -Path $path )
        {
            If(-not $Force)
            {
                Break
            }
            Else
            {
                $path | Remove-Item -Force
            }
        }
    }

    End
    {
        Invoke-WebRequest -Uri $URL -OutFile $path
    }
}

Function Get-HandleRegex
{
    $type = 'System.Text.RegularExpressions.Regex'
    $options = [System.Text.RegularExpressions.RegexOptions]::IgnoreCase -bor
               [System.Text.RegularExpressions.RegexOptions]::Compiled

    $regex = @{}
    
    $pattern = '^(?<program>\S+)\s+pid:\s*(?<processid>\S+)\s*type:\s*(?<type>\S+)\s+(?:(?<user>[^\s:]+?)\s+)?(?<handle>[a-fA-F0-9]+?):\s*(?<file>.*)$'
    
    $regex['oneline'] = New-Object -TypeName $type -ArgumentList $pattern,$options

    $pattern = '^(?<program>\S+)\s+pid:\s*(?<processid>\S+)\s*\\<unable to open process>$'

    $regex['unabletoopen'] = New-Object -TypeName $type -ArgumentList $pattern,$options

    $pattern = '^(?<program>\S+)\s+pid:\s*(?<processid>\S+)\s*(?<!type:)(?<user>\S+)$'

    $regex['liststart'] = New-Object -TypeName $type -ArgumentList $pattern,$options

    $pattern = '^\s*(?<handle>\S+?):\s*(?<type>\S+)\s*(?<file>.*)$'

    $regex['listitem'] = New-Object -TypeName $type -ArgumentList $pattern,$options

    $regex
}

Function ConvertFrom-HandleOutput
{
    [cmdletbinding()]

    Param(
        [Parameter( ValueFromPipeline = $True,
                    Position          = 0      )]
        [String]
        $InputObject
    )

    Begin
    {
        $regex = Get-HandleRegex
        $isInList = $False
    }

    Process
    {
        If( $InputObject -match $regex['oneline'] ) {
            New-Object -TypeName psobject -Property $Matches
            $isInList = $False
        }
        ElseIf( $InputObject -match $regex['unabletoopen'] )
        {
            New-Object -TypeName psobject -Property $Matches
            $isInList = $False            
        }
        ElseIf( $InputObject -match $regex['liststart'] )
        {
            $head = $Matches
            $isInList = $True
        }
        ElseIf( $InputObject -match $regex['listitem'] -and $isInList )
        {
            $item = $Matches
            ForEach($key in $head.Keys)
            {
                $item[$key] = $head[$key]
            }
            New-Object -TypeName psobject -Property $item
        }
    }
}

Function Invoke-HandleExeQuery
{
    [cmdletbinding()]

    Param(
        [String]
        $HandlePath,

        [String]
        $SearchPath,

        [Switch]
        $AcceptEULA,

        [Switch]
        $KeepHandleDownload
    )

    Begin
    {
        $handleExe = $HandlePath
        If( -not $handleExe )
        {
            $handleExe = '.\handle.exe'
            Write-Verbose "$HandlePath not defined, assuming .\handle.exe"
        }

        $handleExeExists = Test-Path -Path $handleExe
        
        If($handleExeExists)
        {
            Write-Verbose "handle.exe already exists"
        }
        Else
        {
            Write-Verbose "getting handle.exe"
            Get-HandleExe -Destination (Split-Path -Path $handleExe)
        }

        If($AcceptEULA)
        {
            Set-HanldeEULA -State Accepted
        }
    }

    Process
    {

        If($SearchPath)
        {
            $path = $SearchPath
            Write-Verbose "Searching for path $path"
            If($path.StartsWith('.\') -or $path.StartsWith('..\'))
            {
                Write-Verbose "handle.exe doesn't support relative paths, removing .\ or ..\"
                $path = $path -replace '^\.{1,2}\\(.*)$','$1'
            }

            &$handleExe -u $path | 
            ConvertFrom-HandleOutput
        }
        Else
        {
            &$handleExe |
            ConvertFrom-HandleOutput
        }

    }

    End
    {
        If(-not $KeepHandleDownload -and -not $handleExeExists)
        {
            Remove-Item -Path $handleExe -Force
        }
    }
}

Function Invoke-HandleExeClose
{
    [cmdletbinding()]

    Param(
        [Parameter( Mandatory                       = $True, 
                    ValueFromPipelineByPropertyName = $True )]
        [String]
        $ProcessID,

        [Parameter( Mandatory                       = $True, 
                    ValueFromPipelineByPropertyName = $True )]
        [String]
        $Handle,

        [String]
        $HandlePath,

        [Switch]
        $AcceptEULA,

        [Switch]
        $KeepHandleDownload
    )

    Begin
    {
        $handleExe = $HandlePath
        If( -not $handleExe )
        {
            $handleExe = '.\handle.exe'
            Write-Verbose "$HandlePath not defined, assuming .\handle.exe"
        }

        $handleExeExists = Test-Path -Path $handleExe
        
        If($handleExeExists)
        {
            Write-Verbose "handle.exe already exists"
        }
        Else
        {
            Write-Verbose "getting handle.exe"
            Get-HandleExe -Destination (Split-Path -Path $handleExe)
        }

        If($AcceptEULA)
        {
            Set-HanldeEULA -State Accepted
        }
    }

    Process
    {
        &$handleExe -c $Handle -y -p $ProcessID
    }

    End
    {
        If(-not $KeepHandleDownload -and -not $handleExeExists)
        {
            Remove-Item -Path $handleExe -Force
        }
    }
}