﻿Function Get-AvailableDriveLetter {
    $drives = [Char[]](([Int][Char]"Z")..([Int][Char]"D"))

    $used = Get-PSDrive -PSProvider FileSystem |
    Where-Object { $drives -contains $_.Name } |
    Select-Object -ExpandProperty Name

    If($used) {
        $available = Compare-Object -ReferenceObject $drives -DifferenceObject $used |
        Where-Object { $_.SideIndicator -eq "<=" } |
        Select-Object -ExpandProperty InputObject
    } Else {
        $available = $drives
    }

    Return $available[0]
}

Function Get-Subst {
    $results = Invoke-Expression -Command "Subst.exe"
    If($results) {
        $drives = ForEach($result in $results) {
            $result[0]
        }
        Return $drives
    }
}

Function New-Subst {
    Param(
        [Parameter(
            Mandatory = $True,
            Position = 0
        )]
        [String]
        $Root,

        [Parameter(
            Position = 1
        )]
        [String]
        $Name = (Get-AvailableDriveLetter)
    )

    $result = Invoke-Expression -Command "Subst.exe $Name`: '$Root'"

    If(-not $result) {
        Return $Name
    } Else {
        Throw "Error Creating Subst for $Name"
    }
}

Function Remove-Subst {
    Param(
        [Parameter(
            Position = 0
        )]
        [String[]]
        $Name = (Get-Subst)
    )
    ForEach($n in $Name) {
        Invoke-Expression -Command "Subst.exe $n`: /D"
    }
}

Function Remove-Subst2 {
    Param(
        [Parameter(
            ParameterSetName = 'All',
            Mandatory = $True
        )]
        [Switch]
        $All
    )

    DynamicParam {
        $paramAttrib_Type = @{'TypeName'='System.Management.Automation.ParameterAttribute'}
        $attribCollection_Type = @{'TypeName'='System.Collections.ObjectModel.Collection[System.Attribute]'}
        $validateSet_Type = @{'TypeName'='System.Management.Automation.ValidateSetAttribute'}
        $runtimeParam_Type = @{'TypeName'='System.Management.Automation.RuntimeDefinedParameter'}
        $runtimeParamList_Type = @{'TypeName'='System.Management.Automation.RuntimeDefinedParameterDictionary'}
        
        $paramList = New-Object @runtimeParamList_Type

        $attrib = New-Object @paramAttrib_Type
        $attrib.Mandatory = $True
        $attrib.ParameterSetName = 'Individual'

        $attribCollection = New-Object @attribCollection_Type
        $attribCollection.Add($attrib)

        $arrSet = Get-Subst
        $validateSet = New-Object @validateSet_Type -ArgumentList $arrSet
        $attribCollection.Add($validateSet)

        $param = New-Object @runtimeParam_Type -ArgumentList "DriveLetter","String[]",$attribCollection
        $paramList.Add("DriveLetter", $param)

        return $paramList
    }

    End{
        Switch ($PSCmdlet.ParameterSetName) {
            'Individual' {
                $DriveLetter = $PSBoundParameters['DriveLetter']
            }
            'All' {
                $DriveLetter = Get-Subst
            }
        }

        ForEach($letter in $DriveLetter) {
            Invoke-Expression -Command "Subst.exe $letter`: /D"
        }
    }
}

Function Get-LongPathErrorParent {
[CmdletBinding()]
    Param(
        [Parameter(
            ValueFromPipeline = $True,
            Mandatory = $True,
            Position = 0
        )]
        [String]
        $Path
    )
    Write-Verbose "Analyzing $Path"
    If($children) {
        Clear-Variable -Name 'children'
    }
    If($directories) {
        Clear-Variable -Name 'directories'
    }
    Try{
        Write-Verbose "Attempting to get children of $Path"
        $children = Get-ChildItem -Path $Path -ErrorAction Stop
    } Catch [System.IO.PathTooLongException] {
        Write-Verbose "Found Path Too Long"
        Return $Path
    } Catch {
        $error[0]
    }

    Write-Verbose "No long path errors for children of $Path"

    If($children) {
        $directories = $children | 
        Where-Object {$_.PSIsContainer} | 
        Select-Object -ExpandProperty FullName
    }

    If($directories) {
        Write-Verbose "Children directories were found, recursing."
        $finds = ForEach($directory in $directories) {
            Write-Verbose "Recusing into $directory"
            Get-LongPathErrorParent -Path $directory
        }
    }

    If($finds) {
        Return $finds
    }
}
